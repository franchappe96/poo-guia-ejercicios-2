package ejercicio1;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Socio {
    private int dni;
    private String nombre;
    private String telefono;
    private String email;
    private Suscripcion suscripcion;
    private Credencial credencial;
    private LocalDate fechaAlta;

    // Constructor con datos básicos
    public Socio(int dni, String nombre, String suscripcion, String fecha) {
        this(dni, nombre, suscripcion, fecha, "", "");
    }
    //Constructor completo
    public Socio(int dni, String nombre, String suscripcion, String fecha, String telefono, String email) {
        setDni(dni);
        setNombre(nombre);
        setSuscripcion(suscripcion);
        setTelefono(telefono);
        setEmail(email);
        setFechaAlta(fecha);
        credencial = new Credencial(this, this.getSuscripcion());
    }
    private void setFechaAlta(String fecha) {
        try{
            fechaAlta = LocalDate.parse(fecha);
        }
        catch (Exception e) {
            this.fechaAlta = LocalDate.now();
        }
    }
    public void setDni(int dni) {
        this.dni = dni;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setSuscripcion(String suscripcion) {
        try {
            this.suscripcion = Suscripcion.valueOf(suscripcion);
        }
        catch (Exception e) {
            System.out.println("No existe la suscripción que desea asignarle al socio.");
        }
    }
    public String getSuscripcion() {
        return suscripcion.toString();
    }
    public int getDni() {
        return dni;
    }
    public String getNombre() {
        return nombre;
    }
    public String getTelefono() {
        return telefono;
    }
    public String getEmail() {
        return email;
    }
    public String getFechaAlta() {
        return fechaAlta.toString();
    }

}
