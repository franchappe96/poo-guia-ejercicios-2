package ejercicio1;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Club c = new Club();
        Socio s1 = new Socio(11111111, "Socio 1", "DESTACADA", "2023-09-22");
        Socio s2 = new Socio(22222222, "Socio 2", "INTERMEDIA", "2023-09-29");
        Socio s3 = new Socio(33333333, "Socio 3", "BASICA", "2023-08-11");
        c.afiliarSocio(s1);
        c.afiliarSocio(s2);
        c.afiliarSocio(s3);
        //System.out.println(c.reporteMensual());
        c.agregarActividad("Gimnasio", "BASICA");
        c.agregarActividad("Pileta", "BASICA");
        c.agregarActividad("Cancha de Fútbol", "INTERMEDIA");
        c.agregarActividad("Cancha de Tenis", "INTERMEDIA");
        c.agregarActividad("Centro Médico", "DESTACADA");
        System.out.println(c.getActividades());
        System.out.println(c.getActividadesPorTipo("INTERMEDIA"));
        System.out.println(c.getListadoSociosPorTipo("DESTACADA"));
    }

}
