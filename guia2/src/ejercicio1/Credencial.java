package ejercicio1;

import java.time.LocalDate;

public class Credencial {
    private Socio titular;
    private Suscripcion tipo = Suscripcion.BASICA;
    private LocalDate vencimiento;

    public Credencial(Socio titular, String tipoSuscripcion) {
        setTitular(titular);
        setTipo(tipoSuscripcion);
        setVencimiento(titular.getFechaAlta());
    }
    public void setVencimiento(String fechaVencimiento) {
        vencimiento = LocalDate.parse(fechaVencimiento).plusMonths(1);
    }
    public void setTipo(String suscripcion) {
        try {
            this.tipo = Suscripcion.valueOf(suscripcion);
        }
        catch (Exception e) {
            System.out.println("No existe la suscripción que desea asignarle al socio.");
        }
    }
    public void setTitular(Socio titular) {
        this.titular = titular;
    }
    public LocalDate getVencimiento() {
        return vencimiento;
    }
    public boolean isVencida() {
        return getVencimiento().isAfter(LocalDate.now());
    }

}
