package ejercicio1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class Club {
    private String nombre;
    private ArrayList<Actividad> actividades = new ArrayList<Actividad>();
    private ArrayList<Socio> socios = new ArrayList<Socio>();
    public void setNombre(String n) {
        nombre = n;
    }
    public int buscarActividad(String actividad) {
        int posicion = 0;
        boolean encontrado = false;
        while(!encontrado && posicion < actividades.size()) {
            if(actividades.get(posicion).getNombre() == actividad) {
                encontrado = true;
            }
            posicion++;
        }
        return posicion;
    }
    public void agregarActividad(String actividad, String suscripcion) {
        if(buscarActividad(actividad) == actividades.size()) {
            Actividad nuevaActividad = new Actividad(actividad, suscripcion);
            actividades.add(nuevaActividad);
        }
        else {
            System.out.println("Ya existe la actividad que desea agregar.");
        }
    }
    public void quitarActividad(String actividad) {
        int posicion = buscarActividad(actividad);
        if(posicion != actividades.size()) {
            actividades.remove(posicion);
        }
        else {
            System.out.println("No existe la actividad que desea eliminar.");
        }
    }
    public String getNombre() {
        return nombre;
    }
    public void afiliarSocio(Socio socio) {
        if(!socios.contains(socio)) {
            socios.add(socio);
            System.out.println("El socio fue afiliado correctamente.");
        }
        else {
            System.out.println("El socio ya se encuentra afiliado al club.");
        }
    }
    public void desafiliarSocio(Socio socio) {
        if(socios.contains(socio)) {
            socios.remove(socio);
            System.out.println("El socio fue desafiliado correctamente.");
        }
        else {
            System.out.println("La persona que desea desafiliar no es socio del club.");
        }
    }
    public String reporteMensual() {
        String reporte = "\t\t-----REPORTE MENSUAL DE NUEVOS AFILIADOS-----\n" +
                "DNI; Nombre y apellido; Tipo de suscripción; Fecha de alta\n";
        LocalDate hoy = LocalDate.now();
        for(Socio socio: socios) {
            LocalDate fecha = LocalDate.parse(socio.getFechaAlta());
            if(fecha.getMonth() == hoy.getMonth() && fecha.getYear() == hoy.getYear()) {
                reporte += socio.getDni() + "; " + socio.getNombre() + "; " + socio.getSuscripcion() + "; " +socio.getFechaAlta() + "\n";
            }
        }
        return reporte;
    }
    public String getActividades() {
        String s = "";
        for(Actividad actividad: actividades) {
            s += actividad.getNombre() + " - " + actividad.getSuscripcion() + "\n";
        }
        return s;
    }
    public String getActividadesPorTipo(String tipoSuscripcion) {
        String s = "";
        for(Actividad actividad: actividades) {
            if(actividad.getSuscripcion() == tipoSuscripcion) {
                s += actividad.getNombre() + "\n";
            }
        }
        return s;
    }
    public String getListadoSociosPorTipo(String tipoSuscripcion) {
        String s = "";
        for(Socio socio: socios) {
            if(socio.getSuscripcion() == tipoSuscripcion) {
                s += socio.getDni() + " - " + socio.getNombre() + " - " + socio.getFechaAlta() + "\n";
            }
        }
        return s;
    }

}
