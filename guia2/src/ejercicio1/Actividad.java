package ejercicio1;

public class Actividad {
    private String nombre;
    private Suscripcion tipoSuscripcion = Suscripcion.BASICA;

    public Actividad(String nombre, String tipoSuscripcion) {
        setNombre(nombre);
        setTipoSuscripcion(tipoSuscripcion);
    }
    public String getNombre() {
        return nombre;
    }
    public String getSuscripcion() {
        return tipoSuscripcion.name();
    }
    public void setNombre(String actividad) {
        nombre = actividad;
    }
    public void setTipoSuscripcion(String suscripcion) {
        try {
            this.tipoSuscripcion = Suscripcion.valueOf(suscripcion);
        }
        catch (Exception e) {
            System.out.println("No existe la suscripción que desea asignarle a la actividad.");
        }
    }

}
