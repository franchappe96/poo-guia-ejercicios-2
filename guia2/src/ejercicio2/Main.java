package ejercicio2;

public class Main {
    public static void main(String[] args) {
        // Se crea la agencia
        Agencia sunBeach = new Agencia("SunBeach");

        // Se crean los proveedores
        Proveedor jetSmart = new Proveedor("JetSmart", Clasificacion.TRANSPORTE);
        Proveedor sheraton = new Proveedor("Sheraton Hotel", Clasificacion.HOSPEDAJE);
        Proveedor excursionesPepe = new Proveedor("Pepe Excursiones", Clasificacion.EXCURSION);

        // Se agregan servicios
        jetSmart.agregarServicio("Iguazú", "Vuelo directo a Puerto Iguazú", 25000, true);
        sheraton.agregarServicio("Iguazú", "Habitación para dos personas", 12000, true);
        excursionesPepe.agregarServicio("Iguazú", "Visita a las Cataratas del Iguazú", 5000, true);
        excursionesPepe.agregarServicio("Iguazú", "Visita a la Aripuca", 3500, true);
        jetSmart.agregarServicio("Salta", "Vuelo directo a Salta (Capital)", 22000, true);
        sheraton.agregarServicio("Salta", "Habitación para tres personas", 18000, true);
        excursionesPepe.agregarServicio("Salta", "Visita a bodegas", 4500, true);
        excursionesPepe.agregarServicio("Salta", "Paseo gastronomico", 6000, true);
        sunBeach.agregarProveedor(jetSmart);
        sunBeach.agregarProveedor(sheraton);
        sunBeach.agregarProveedor(excursionesPepe);

        // Se crean los clientes
        Cliente cliente1 = new Cliente(11111111, "Cliente 1");
        Cliente cliente2 = new Cliente(22222222, "Cliente 2");
        Cliente cliente3 = new Cliente(33333333, "Cliente 3");

        // Se agregan los clientes
        sunBeach.agregarCliente(cliente1);
        sunBeach.agregarCliente(cliente2);
        sunBeach.agregarCliente(cliente3);

        // Listado de servicios
        System.out.println(sunBeach.listadoProveedores());

        sunBeach.generarPaquete("Iguazú");
        sunBeach.generarPaquete("Salta");
        System.out.println(sunBeach.mostrarPaquetes());
        System.out.println(sunBeach.mostrarClientes());

        // Compras
        Venta v = cliente1.seleccionarPaquete(sunBeach, 2);
        sunBeach.agregarVenta(v);
        System.out.println(cliente1.mostrarCompras());

        // Ventas
        System.out.println(sunBeach.mostrarVentas());
    }
}
