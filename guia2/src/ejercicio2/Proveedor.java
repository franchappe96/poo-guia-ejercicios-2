package ejercicio2;

import java.util.ArrayList;

public class Proveedor {
    private String nombre;
    private final Clasificacion tipo;
    private ArrayList<Servicio> servicios = new ArrayList<Servicio>();
    public Proveedor(String nombre, Clasificacion clasificacion) {
        setNombre(nombre);
        tipo = clasificacion;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void agregarServicio(String destino, String descripcion, double precio, boolean estado) {
        if(!existeServicio(destino, descripcion)) {
            Servicio nuevoServicio = new Servicio(destino, descripcion, precio, estado);
            servicios.add(nuevoServicio);
        }
        else {
            System.out.println("Ya existe el servicio que desea agregar");
        }
    }
    private boolean existeServicio(String destino, String descripcion) {
        boolean existe = false;
        int i = 0;
        Servicio servicio;
        while(!existe && i < servicios.size()) {
            servicio = servicios.get(i);
            if (servicio.getDestino() == destino && servicio.getDescripcion() == descripcion) {
                existe = true;
            }
            i++;
        }
        return existe;
    }
    public String getNombre() {
        return nombre;
    }
    public String getTipo() {
        return tipo.toString();
    }
    public Servicio buscarServicio(String destino) {
        Servicio servicioRes = null;
        for(Servicio servicio: servicios) {
            if(servicio.getDestino() == destino && servicio.getDisponible()) {
                servicioRes = servicio;
            }
        }
        return servicioRes;
    }
    public ArrayList<Servicio> getServicios() {
        return servicios;
    }

}

