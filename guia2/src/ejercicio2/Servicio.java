package ejercicio2;

public class Servicio {
    private final String destino;
    private String descripcion;
    private boolean disponible;
    private double precio;

    public Servicio(String destino, String descripcion, double precio, boolean disponible) {
        this.destino = destino;
        setDisponible(disponible);
        setPrecio(precio);
        setDescripcion(descripcion);
    }
    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public String getDestino() {
        return destino;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public boolean getDisponible() {
        return disponible;
    }

}
