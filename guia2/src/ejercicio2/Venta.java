package ejercicio2;

import java.time.LocalDate;

public class Venta {
    private LocalDate fecha;
    private Cliente cliente;
    private Paquete paquete;

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    public void setPaquete(Paquete paquete) {
        this.paquete = paquete;
    }

    public String toString() {
        return cliente.getDni() + " - " + paquete.toString() + " - " + fecha.toString();
    }

}
