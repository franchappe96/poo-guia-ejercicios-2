package ejercicio2;

import java.time.LocalDate;
import java.util.ArrayList;

public class Cliente {
    private int dni;
    private String nombre;
    private ArrayList<Venta> compras = new ArrayList<Venta>();

    public Cliente(int dni, String nombre) {
        setDni(dni);
        setNombre(nombre);
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }
    public int getDni() {
        return dni;
    }
    public Venta seleccionarPaquete(Agencia agencia, int codigo) {
        Paquete paquete = agencia.buscarPaquetePorCodigo(codigo);
        Venta venta = new Venta();
        if(paquete != null) {
            venta.setCliente(this);
            venta.setPaquete(paquete);
            venta.setFecha(LocalDate.now());
            compras.add(venta);
        }
        return venta;
    }

    public String mostrarCompras() {
        String s = "";
        for(Venta compra: compras) {
            s += compra.toString() + "\n";
        }
        return s;
    }

}
