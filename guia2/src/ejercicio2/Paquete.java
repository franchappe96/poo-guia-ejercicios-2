package ejercicio2;

import java.util.ArrayList;

public class Paquete {
    private final String destino;
    private Servicio transporte;
    private int id;
    private static int numeroPaquete = 0;
    private ArrayList<Servicio> excursiones = new ArrayList<Servicio>();
    private Servicio hospedaje;
    private boolean disponible = false;

    public Paquete(String destino) {
        numeroPaquete++;
        this.destino = destino;
        this.id = numeroPaquete;
    }
    public void setTransporte(Servicio transporte) {
        this.transporte = transporte;
    }
    public void setHospedaje(Servicio hospedaje) {
        this.hospedaje = hospedaje;
    }
    public void agregarExcursiones(Servicio excursion) {
        if(!excursiones.contains(excursion)) {
            excursiones.add(excursion);
        }
    }
    public String getDestino() {
        return destino;
    }
    public Servicio getTransporte() {
        return transporte;
    }
    public Servicio getHospedaje() {
        return hospedaje;
    }
    public boolean isCompleto() {
        return getDestino() != null && getTransporte() != null && getHospedaje() != null && !excursiones.isEmpty();
    }

    public int cantidadExcursiones() {
        return excursiones.size();
    }

    public String getExcursiones() {
        String s = "Excursiones: ";
        for(Servicio excursion: excursiones) {
            s += excursion.getDescripcion() + "; ";
        }
        return s;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        return transporte.getDescripcion() + " - " + hospedaje.getDescripcion() + " - " +destino;
    }

}
