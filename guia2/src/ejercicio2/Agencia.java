package ejercicio2;

import java.util.ArrayList;

public class Agencia {
    private ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
    private ArrayList<Paquete> paquetes = new ArrayList<Paquete>();
    private ArrayList<Cliente> clientes = new ArrayList<Cliente>();
    private ArrayList<Venta> ventas = new ArrayList<Venta>();
    private String nombre;
    public Agencia(String nombre) {
        setNombre(nombre);
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void agregarProveedor(Proveedor proveedor) {
        if(!proveedores.contains(proveedor)) {
            proveedores.add(proveedor);
        }
        else{
            System.out.println("El proveedor que desea agregar fue agregado anteriormente");
        }
    }
    public String listadoProveedores() {
        String s = "";
        for (Proveedor proveedor: proveedores) {
            s += proveedor.getNombre() + " - " + proveedor.getTipo() + "\n";
        }
        return s;
    }
    public void agregarCliente(Cliente cliente) {
        if(!clientes.contains(cliente)) {
            clientes.add(cliente);
        }
        else {
            System.out.println("El cliente que desea agregar fue agregado anteriormente");
        }
    }
    public void generarPaquete(String destino) {
        // Se genera un nuevo paquete
        Paquete nuevoPaquete = new Paquete(destino);
        // Se intenta completar el paquete
        for(Proveedor proveedor: proveedores) {
            if(nuevoPaquete.getTransporte() == null && proveedor.getTipo() == "TRANSPORTE") {
                Servicio transporte = proveedor.buscarServicio(destino);
                nuevoPaquete.setTransporte(transporte);
            }
            if(nuevoPaquete.getHospedaje() == null && proveedor.getTipo() == "HOSPEDAJE") {
                Servicio hospedaje = proveedor.buscarServicio(destino);
                nuevoPaquete.setHospedaje(hospedaje);
            }
            if(proveedor.getTipo() == "EXCURSION") {
                ArrayList<Servicio> servicios = proveedor.getServicios();
                for(Servicio excursion: servicios) {
                    if (excursion.getDestino() == destino) {
                        nuevoPaquete.agregarExcursiones(excursion);
                    }
                }
            }
        }
        if(nuevoPaquete.isCompleto()) {
            paquetes.add(nuevoPaquete);
            System.out.println("Se ha agregado el paquete correctamente");
        }
        else {
            System.out.println("No se logró completar ningún paquete con el destino elegido");
        }

    }
    public String mostrarPaquetes() {
        String s = "";
        for(Paquete paquete: paquetes) {
            s += paquete.getId() + " - " + paquete.getTransporte().getDescripcion() + " - " + paquete.getHospedaje().getDescripcion() + " - " + paquete.getDestino() + "\n"
            + paquete.getExcursiones() + "\n----------------------------------------------------\n";
        }
        return s;
    }
    public String mostrarClientes() {
        String s = "";
        for(Cliente cliente: clientes) {
            s += cliente.getNombre() + " - DNI: " + cliente.getDni() + "\n";
        }
        return s;
    }
    public Paquete buscarPaquetePorCodigo(int codigo) {
        boolean encontrado = false;
        int i = 0;
        Paquete paqueteRes = null;
        while(!encontrado && i < paquetes.size()) {
            Paquete paquete = paquetes.get(i);
            if(paquete.getId() == codigo) {
                encontrado = true;
                paqueteRes = paquete;
            }
            i++;
        }
        return paqueteRes;
    }
    public void agregarVenta(Venta venta) {
        if(venta != null) {
            ventas.add(venta);
        }
    }
    public String mostrarVentas() {
        String s = "";
        for(Venta venta: ventas) {
            s += venta.toString() + "\n";
        }
        return s;
    }

}
